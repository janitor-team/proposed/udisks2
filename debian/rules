#!/usr/bin/make -f

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

%:
	dh $@

override_dh_auto_configure:
	dh_auto_configure -- \
		--enable-bcache \
		--enable-btrfs \
		--enable-lvm2 \
		--enable-lvmcache \
		--disable-vdo \
		--enable-zram \
		--enable-gtk-doc=$(if $(filter nodoc,$(DEB_BUILD_PROFILES)),no,yes) \
		--enable-fhs-media \
		--with-systemdsystemunitdir=/lib/systemd/system

execute_after_dh_auto_install:
	# install Apport hook on Ubuntu
	if dpkg-vendor --is ubuntu; then \
	    install -m 644 -D debian/local/apport-hook.py debian/udisks2/usr/share/apport/package-hooks/udisks2.py; \
	fi

execute_before_dh_install:
	find debian/tmp -name '*.la' -print -delete
	find debian/tmp -name 'libudisks2_*.a' -print -delete

# Ubuntu is hesitant about exfatprogs in default install
# https://launchpad.net/bugs/1649537
override_dh_gencontrol:
ifneq ($(shell dpkg-vendor --query vendor),Ubuntu)
	dh_gencontrol -- -Vexfat:Recommends='exfatprogs'
else
	dh_gencontrol -- -Vexfat:Suggests='exfatprogs'
endif
